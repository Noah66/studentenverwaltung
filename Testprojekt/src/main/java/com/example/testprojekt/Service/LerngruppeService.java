package com.example.testprojekt.Service;

import com.example.testprojekt.Entities.Lerngruppe;
import com.example.testprojekt.Entities.Student;
import com.example.testprojekt.Repositories.LerngruppeRepo;
import com.example.testprojekt.Repositories.StudentRepo;
import com.example.testprojekt.Request.LerngruppeRequest;
import org.springframework.stereotype.Service;

@Service
public class LerngruppeService {

    private final LerngruppeRepo lerngruppeRepo;
    private final StudentRepo studentRepo;

    public LerngruppeService(LerngruppeRepo lerngruppeRepo, StudentRepo studentRepo) {
        this.lerngruppeRepo = lerngruppeRepo;
        this.studentRepo = studentRepo;
    }

    public String erzeugeLerngruppe(LerngruppeRequest lerngruppeRequest, int id){

        Student student = studentRepo.findById(id);
        Lerngruppe lerngruppe = new Lerngruppe(lerngruppeRequest.getName(), lerngruppeRequest.getBeschreibung());
        lerngruppe.erzeuger(student);
        lerngruppeRepo.save(lerngruppe);

        return "Lerngruppe wurde erfolgreich angelegt";
    }

    public String studentHinzufugen(int gruppenId, int studentenId){
        Lerngruppe lerngruppe = lerngruppeRepo.findById(gruppenId);
        Student student = studentRepo.findById(studentenId);
        lerngruppe.studentenHinzufugen(student);
        lerngruppeRepo.save(lerngruppe);

        return "Der Student wurde erfolgreich hinzugefügt";
    }
}
