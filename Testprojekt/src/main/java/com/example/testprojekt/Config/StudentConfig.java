package com.example.testprojekt.Config;


import com.example.testprojekt.Entities.Student;
import com.example.testprojekt.Repositories.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class StudentConfig {

    @Autowired
    private final StudentRepo studentRepo;

    public StudentConfig(StudentRepo studentRepo) {
        this.studentRepo = studentRepo;
    }

    @Bean
    CommandLineRunner commandLineRunner(){
        return args -> {
            Student student1 = new Student(
                    "Alex",
                    "Sommer",
                    "Alex@Sommer.de",
                    12345678,
                    "passwort1234",
                    null
            );

            Student student2 = new Student(
                    "Iris",
                    "Bauer",
                    "Iris@Bauer.de",
                    12345677,
                    "Iris124",
                    null
            );

            if(studentRepo.findAll().size() == 0){
                studentRepo.saveAll(
                        List.of(student1, student2)
                );
            }
        };
    }

}
