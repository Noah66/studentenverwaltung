package com.example.testprojekt.Repositories;

import com.example.testprojekt.Entities.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentRepo extends JpaRepository<Student, Integer> {

    Optional<Student> findByEmail(String email);

    Optional<Student> findByMatrikelnummer(int matrikelnummer);

    Optional<Student> findByEmailAndPasswort(String email, String passwort);

    Student findById(int id);
}
